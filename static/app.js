/**
 * Created by hardkiller on 05.04.16.
 */

var app = angular.module("sampleChat", []);
app.controller("appCtrl", function($scope) {
    $scope.message = "";

    $scope.messages = [];

    var socket = io();


    socket.on ('room', function (msg){
        $scope.messages.push (msg);
        console.log ('message received');
        $scope.$apply();
    });

    $scope.sendMessage = function () {

        var msg = { msg:  $scope.message, user: "qweqwe"};

        //$scope.messages.push(msg);

        socket.emit ('message', msg);
    }
});
