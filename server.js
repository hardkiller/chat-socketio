/**
 * Created by hardkiller on 05.04.16.
 */
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');

app.use('/bower_components', express.static(path.join('./bower_components/')));

app.use(express.static(path.join(__dirname, './static/')));


io.on('connection', function(socket){
console.log('a user connected');
    socket.on ('message', function (msg){
        console.log (msg);
        io.emit('room',msg);
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});